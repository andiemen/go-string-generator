# Go String Generator

Generate random string, send it to an encryptor service to be encrypted and return the reslt.

After cloning, run 
- "docker build -t golang-string-generator-api ."
- "docker run -p 8081:9091 golang-string-generator-api"

Note; Ensure string-encryptor api is running to get encrypted string.