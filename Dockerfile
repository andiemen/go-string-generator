FROM golang:latest

LABEL maintainer="Peter <andiemen@gmail.com>"

WORKDIR /app

COPY . .

RUN go build

CMD ["./go-string-generator"]