package main

import (
	"context"
	"gitlab.com/andiemen/go-string-generator/handlers"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"
)

func main() {
	// Set Environment Variables
	os.Setenv("ENCRYPTOR_ADDRESS", "http://127.0.0.1:8080/encrypt")
	os.Setenv("ADDRESS", ":9091")
	os.Setenv("IDLE_TIMEOUT", "120")
	os.Setenv("READ_TIMEOUT", "1")
	os.Setenv("WRITE_TIMEOUT", "1")
	os.Setenv("TERM_MSG", "Received terminate, graceful shutdown")
	os.Setenv("TERM_TIMEOUT", "30")

	l := log.New(os.Stdout, "string-encryptor-api", log.LstdFlags)
	stringGenerator := handlers.RandStringBytesMaskImprSrcSB(1)
	sm := http.NewServeMux()
	sm.Handle("/generate", &stringGenerator)

	idleTimeout, _ := time.ParseDuration(os.Getenv("IDLE_TIMEOUT"))
	readTimeout, _ := time.ParseDuration(os.Getenv("READ_TIMEOUT"))
	writeTimeout, _ := time.ParseDuration(os.Getenv("WRITE_TIMEOUT"))

	s := &http.Server{
		Addr: os.Getenv("ADDRESS"),
		Handler: sm,
		IdleTimeout:  idleTimeout * time.Second,
		ReadTimeout: readTimeout * time.Second,
		WriteTimeout: writeTimeout * time.Second,
	}

	go func() {
		err := s.ListenAndServe()
		if err != nil {
			l.Fatal(err)
		}
	}()

	sigChan := make(chan os.Signal)
	signal.Notify(sigChan, os.Interrupt)
	signal.Notify(sigChan, os.Kill)

	sig := <- sigChan
	l.Println(os.Getenv("TERM_MSG"), sig)
	timeOut, _ := time.ParseDuration(os.Getenv("TERM_TIMEOUT"))
	tc, _ := context.WithTimeout(context.Background(), timeOut* time.Second)
	s.Shutdown(tc)

}
