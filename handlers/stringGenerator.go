package handlers

import (
	"encoding/json"
	"io"
	"math/rand"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

type RandomString struct {
	Length int `json:"string_length"`
	Output string `json:"generated_string"`
	Encrypted []byte `json:"encrypted_string"`
	Error error `json:"error,omitempty"`
}

type Encryption struct {
	Input string `json:"input"`
	Output []byte `json:"output"`
	Error error `json:"error,omitempty"`
}

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
const (
	letterIdxBits = 6                    // 6 bits to represent a letter index
	letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
	letterIdxMax  = 63 / letterIdxBits   // # of letter indices fitting in 63 bits
)
var src = rand.NewSource(time.Now().UnixNano())

func RandStringBytesMaskImprSrcSB(n int) RandomString {
	sb := strings.Builder{}
	sb.Grow(n)
	// A src.Int63() generates 63 random bits, enough for letterIdxMax characters!
	for i, cache, remain := n-1, src.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = src.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < len(letterBytes) {
			sb.WriteByte(letterBytes[idx])
			i--
		}
		cache >>= letterIdxBits
		remain--
	}

	return RandomString {
		Length: n,
		Output: sb.String(),
	}
}

var myClient = &http.Client{Timeout: 10 * time.Second}

func getJson(url string) Encryption {
	r, err := myClient.Get(url)
	if err != nil {
		return Encryption{}
	}
	defer r.Body.Close()
	body, _ := io.ReadAll(r.Body)

	result := Encryption{}
	json.Unmarshal(body, &result)
	return result
}

func (rs *RandomString) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	n, ok := r.URL.Query()["n"]
	if !ok || len(n[0]) < 1 {
		http.Error(rw, "Please specify the number of characters in the string. e.g. ?n=10", http.StatusBadRequest)
		return
	}
	stringLength, _ := strconv.ParseInt(n[0], 10, 8)
	if stringLength < 1 {
		http.Error(rw, "The number of characters in the string must be at least 1. e.g. ?n=10", http.StatusBadRequest)
		return
	}
	randString := RandStringBytesMaskImprSrcSB(int(stringLength))

	encryptionURL := os.Getenv("ENCRYPTOR_ADDRESS")+"?text="+randString.Output
	encryption := getJson(encryptionURL)
	// Decided to return "null" in any case of error.
	// if encryption.Output == nil {
	// 	http.Error(rw, "Couldn't encrypt string", http.StatusInternalServerError)
	// 	return
	// }
	randString.Encrypted = encryption.Output

	json.NewEncoder(rw).Encode(randString)
}